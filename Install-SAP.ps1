﻿[CmdletBinding()]
param(
    $ServerName,
    $ServerIP,
    $B1Version
)

try {
    $hostEntry = "$ServerIP`t$ServerName"

    Add-Content -Path C:\windows\system32\drivers\etc\hosts -Value $hostEntry

    add-type -AssemblyName microsoft.VisualBasic
    add-type -AssemblyName System.Windows.Forms

    $path = "D:\B1HANA_MEDIA\B1HANA_$B1Version"
    $hdbClientPath = Join-Path $path "Windows\SAP_HANA_CLIENT64"

    #Install HDBClient

    CD $hdbClientPath
    .\hdbinst.exe --path=C:\sap\hdbclient

    Start-Sleep -Seconds 120

    $hdbTitle = Get-Process hdbinst | select -ExpandProperty MainWindowTitle

    [Microsoft.VisualBasic.Interaction]::AppActivate($hdbTitle)
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")

    #Install B1 Client
    switch($B1Version) {
        '920' {
            $b1Path = Join-Path $path "Common\*\Packages.x64\Client" -Resolve
        }
        '910' {
            $b1Path = Join-Path $path "Common\*\Packages.x64\Client" -Resolve
        }
        '930'{
            $b1Path = Join-Path $path "Common\*\Packages.x64\Client" -Resolve
        }
    }

    CD $b1Path
    .\Setup.exe

    Start-Sleep -Seconds 10

    [Microsoft.VisualBasic.Interaction]::AppActivate("SAP Business One Client (64-bit) - InstallShield Wizard")
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")

    Start-Sleep -Seconds 60

    [Microsoft.VisualBasic.Interaction]::AppActivate("SAP Business One Client (64-bit) - InstallShield Wizard")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -Seconds 5
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -Seconds 180
    [Microsoft.VisualBasic.Interaction]::AppActivate("SAP Business One DI API (64-bit) - InstallShield Wizard")
    [System.Windows.Forms.SendKeys]::SendWait($ServerName)
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("40000")
    Start-Sleep -Seconds 1
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    Start-Sleep -Seconds 5
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")


    Start-Sleep -Seconds 360


    Remove-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name AutoAdminLogon
    Remove-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultUserName
    Remove-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultPassword

    Write-AWSQuickStartStatus
}
catch {
    $_ | Write-AWSQuickStartException
}
