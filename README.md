# SAPB1-HANA-CloudFormation

Entire instructions for setting up SAP B1 Hana ecosystem -> https://docs.aws.amazon.com/quickstart/latest/sap-b1-hana/welcome.html

Template will be deployed to the Test AWS account US-West-2.

Requirements:
- Step 2
Use the AWS CloudFormation template sap-b1-infrastructure-rdp.template to create new CloudFormation with AWS resources (that will be used in step 5). https://docs.aws.amazon.com/quickstart/latest/sap-b1-hana/step2.html

- Step 3
Create the volume/snapshots that hosts the downloaded SAP B1 Hana files -> (Currently using the snapshot snap-00c50b1e46e46c4ee) https://docs.aws.amazon.com/quickstart/latest/sap-b1-hana/step3.html

- Step 4
Create the CloudFormation (using Template_1_SAP_B1.template file) to deploy SAP B1 Hana on. Please please please use the generated AWS resources from the CloudFormation generated in Step 2

Debugging:
When creating the stack, disable automatic rollback to retain the instances and logs.
Try to figure out setting up CloudFormationLogs (Line 1242).
